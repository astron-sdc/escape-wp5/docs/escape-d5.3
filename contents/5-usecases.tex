\section{Use Cases}
\label{sec:usecases}

The \gls{ESCAPE} project has formalized its collection of use cases on the project platform\footnote{\url{https://project.escape2020.de}}.
Each of the \glspl{ESFRI} involved in the project has been updating and modernizing its use cases and incorporating them into this platform, a task that is still ongoing at time of writing.
This section focuses on those use cases which have been included in the platform to date, considering how each one can be addressed by the current or envisioned capabilities of \pgls{ESAP}.
Additions and refinements to the material presented here are expected as new use cases are added; these updates will be collated within the platform itself, rather than by updates to this document.
A full description of exactly how each use case might be executed is out of scope for this document; instead, we focus on highlighting those areas of the use cases where \pgls{ESAP} may be expected to contribute, and how that relates to the \pgls{ESAP} vision described in \cref{sec:vision}.

Use cases are organized by the originating \gls{ESFRI}.

\subsection{\glsentrydesc{CTA}}

\subsubsection{CTA001: Long haul ingestion and replication}

This use case has no direct impact on \pgls{ESAP} development.

\subsubsection{CTA002: Data Reprocessing}

Each of the stages of the use case may be addressed by ESAP as follows:

\begin{enumerate}

\item{\emph{Raw data is identified on tape  via metadata e.g.\ using \texttt{getMetaData} method.}
  \begin{itemize}
    \item{\pgls{ESAP} provides flexible routines for searching archives via metadata, as described in \cref{sec:vision:capabilities:data}.}
    \item{Currently, \pgls{ESAP} does not integrate with the \gls{CTA} archive, but this functionality is planned.}
  \end{itemize}
}

\item{\emph{Data volume is calculated.}
  \begin{itemize}
    \item{\pgls{ESAP} does not directly provide mechanisms for calculating aggregates over selected data, but extension to address this part of the use case is likely straightforward.}
    \item{Arbitrary calculations based on metadata are possible by routing the workflow through a Jupyter notebook using the functionality described in \cref{sec:vision:capabilities:ida}.}
  \end{itemize}
}

\item{\emph{Data is staged from tape storage to temporary disk.}
  \begin{itemize}
    \item{Data staging functionality is planned for \pgls{ESAP}, as described in \cref{sec:vision:capabilities:data}, but has not yet been implemented.}
  \end{itemize}
}

\item{\emph{Data is reprocessed using \gls{CTA} pipeline software via the workload management system using a cache area for on-the fly, transient data products.}
  \begin{itemize}
    \item{Batch processing functionality is planned for \pgls{ESAP}, as described in \cref{sec:vision:capabilities:batch}, but has not yet been implemented.}
  \end{itemize}
}

\item{\emph{Final data products are verified.}
  \begin{itemize}
    \item{Details of the verification process are not specified in this use case, but it seems like that \pgls{ESAP}'s \gls{IDA} functionality, described in \S\ref{sec:vision:capabilities:ida}, would be appropriate.}
  \end{itemize}
}

\item{\emph{Cache and temporary data is cleared.}
  \begin{itemize}
    \item{No direct impact on \gls{ESAP}, although potentially an interface could be added to \gls{ESAP} to enable convenient management of the relevant data.}
  \end{itemize}
}

\item{\emph{Ingest the resulting new data into the data lake.}
  \begin{itemize}
    \item{Data can be transmitted from analysis systems to the data lake without the direct involvement of \gls{ESAP}. However, \gls{ESAP} may contribute to providing it with an appropriate \gls{PID}, as described in \cref{sec:vision:capabilities:provenance}.}
  \end{itemize}
}

\item{\emph{Update the corresponding metadata.}
  \begin{itemize}
    \item{No direct connection to \pgls{ESAP}.}
  \end{itemize}
}

\end{enumerate}

In short, all aspects of this use case which are of relevance to \pgls{ESAP} are addressed in the project's vision (\S\ref{sec:vision}), although further work is required to complete delivery of these capabilities.

\subsubsection{CTA003: Generation of Instrument Response Function}

Each of the stages of the use case may be addressed by ESAP as follows:

\begin{enumerate}

\item{\emph{User input of parameters. (Time period, model, systematic uncertainty)}
  \begin{itemize}
    \item{This would take place in an \gls{IDA} environment provisioned through \pgls{ESAP}.}
  \end{itemize}
}

\item{\emph{Validation of parameters.}
  \begin{itemize}
    \item{This would take place in an \gls{IDA} environment provisioned through \pgls{ESAP}.}
    \item{Appropriate software and services would be sourced from the \gls{OSSR} and included in the environment by \pgls{ESAP}.}
  \end{itemize}
}

\item{\emph{Confirm valid simulation does not already exist.}
  \begin{itemize}
    \item{This would take place in an \gls{IDA} environment provisioned through \pgls{ESAP}.}
    \item{Appropriate software and services would be sourced from the \gls{OSSR} and included in the environment by \pgls{ESAP}.}
  \end{itemize}
}

\item{\emph{Calculate number of required events.}
  \begin{itemize}
    \item{This would take place in an \gls{IDA} environment provisioned through \pgls{ESAP}.}
    \item{Appropriate software and services would be sourced from the \gls{OSSR} and included in the environment by \pgls{ESAP}.}
  \end{itemize}
}

\item{\emph{Search for compute resources.}
  \begin{itemize}
    \item{This will be addressed through \pgls{ESAP}'s service discovery functionality (\cref{sec:vision:capabilities:discovery}).}
  \end{itemize}
}

\item{\emph{Job submission via workflow management system.}
  \begin{itemize}
    \item{This will be addressed through \pgls{ESAP}'s batch computing functionality (\cref{sec:vision:capabilities:batch}).}
  \end{itemize}
}
\item{\emph{Validation of simulated instrument response function.}
  \begin{itemize}
    \item{This will take place in an \gls{IDA} environment provisioned through \pgls{ESAP}.}
    \item{\gls{ESAP}'s data discovery functionality (\cref{sec:vision:capabilities:data}) can be used to identify the appropriate simulated data for use in the \gls{IDA} environment.}
  \end{itemize}
}
\item{\emph{Ingest instrument response function in the data lake with appropriate metadata.}
  \begin{itemize}
    \item{Data can be transmitted from analysis systems to the data lake without the direct involvement of \gls{ESAP}. However, \gls{ESAP} may contribute to providing it with an appropriate \gls{PID}, as described in \cref{sec:vision:capabilities:provenance}.}
  \end{itemize}
}
\item{\emph{Remove simulated data.}
  \begin{itemize}
    \item{No direct impact on \gls{ESAP}, although potentially an interface could be added to \gls{ESAP} to enable convenient management of the relevant data.}
  \end{itemize}
}

\end{enumerate}

In short, all aspects of this use case which are of relevance to \pgls{ESAP} are addressed in the project's vision (\S\ref{sec:vision}), although further work is required to complete delivery of these capabilities.

\subsubsection{CTA004a: Interactive analysis of (simulated) \gls{CTA} science data by a principal investigator}
\label{sec:use:cta:cta004a}

Each of the stages of the use case may be addressed by ESAP as follows:

\begin{enumerate}

\item{\emph{User logs in to \gls{ESAP} and is identified as a \gls{CTA} project principal investigator.}
  \begin{itemize}
    \item{\pgls{ESAP} is integrated with \gls{ESCAPE} project \gls{AAAI}, as described in \cref{sec:vision:capabilities:aa}.}
    \item{\pgls{ESAP} itself does not track project affiliation or access rights, but rather delegates this to the services which directly manage data access.}
  \end{itemize}
}

\item{\emph{Search for (simulated) \gls{CTA} high-level data by project identifier.}
  \begin{itemize}
    \item{Addressed by \cref{sec:vision:capabilities:data}.}
  \end{itemize}
}

\item{\emph{Search for corresponding instrument response function for the data selected.}
  \begin{itemize}
    \item{Addressed by \cref{sec:vision:capabilities:data}.}
    \item{Further integration work is required to make it straightforward to use the outputs of one \pgls{ESAP} query as the inputs to a further query.}
  \end{itemize}
}

\item{\emph{Search for corresponding metadata, log files etc.}
  \begin{itemize}
    \item{Addressed by \cref{sec:vision:capabilities:data}.}
  \end{itemize}
}

\item{\emph{The data can now be analysed in interactive mode using Jupyter.}
  \begin{itemize}
    \item{Addressed by \cref{sec:vision:capabilities:ida,sec:vision:capabilities:discovery}.}
    \item{\pgls{ESAP} will assist by identifying appropriate analysis workflows, identifying \gls{IDA} services capable of executing those workflows, and making it straightforward for the user to access them together with the data.}
  \end{itemize}
}

\end{enumerate}

In short, all aspects of this use case which are of relevance to \pgls{ESAP} are addressed in the project's vision (\S\ref{sec:vision}), although further work is required to complete delivery of these capabilities.

\subsubsection{CTA004b: Batch analysis of (simulated) \gls{CTA} science data by a principal investigator}

This use case proceeds as CTA004a (\cref{sec:use:cta:cta004a}) until step 6, where analysis is performed by a batch processing system rather than interactively.
The same comments apply to \pgls{ESAP}'s role in this workflow, with the exception that batch processing is described by \cref{sec:vision:capabilities:batch} rather than \cref{sec:vision:capabilities:ida}.

\subsubsection{CTA005: Analysis of a (simulated) AGN using a combined workflow (gammapy \& AGNpy)}
\label{sec:use:cta:cta005}

Each of the stages of the use case may be addressed by ESAP as follows:

\begin{itemize}

\item{\emph{User logs in to \gls{ESAP} and discovers data based on their own data rights.}
  \begin{itemize}
    \item{Addressed as per CTA004a (\cref{sec:use:cta:cta004a}).}
  \end{itemize}
}

\item{\emph{Search for and select simulated \gls{CTA} data in the data lake.}
  \begin{itemize}
    \item{Addressed as per CTA004a (\cref{sec:use:cta:cta004a}).}
  \end{itemize}
}

\item{\emph{Search for the corresponding instrument response function for the data selected.}
  \begin{itemize}
    \item{Addressed as per CTA004a (\cref{sec:use:cta:cta004a}).}
  \end{itemize}
}

\item{\emph{Search for the corresponding metadata, log files etc.}
  \begin{itemize}
    \item{Addressed as per CTA004a (\cref{sec:use:cta:cta004a}).}
  \end{itemize}
}

\item{\emph{Analyze the data in an interactive session, including identifying an appropriate workflow and execution service, making data available in the interactive environment, and storing the results.}
  \begin{itemize}
    \item{Addressed as per CTA004a (\cref{sec:use:cta:cta004a}).}
  \end{itemize}
}
\end{itemize}

In short, all aspects of this use case which are of relevance to \pgls{ESAP} are addressed in the project's vision (\S\ref{sec:vision}), although further work is required to complete delivery of these capabilities.

\subsubsection{CTA006: Combined \gls{CTA} + KM3NeT Analysis}

The workflow for this use case is the same as that for CTA005 (\cref{sec:use:cta:cta005}).

\subsubsection{CTA007: Search for public \gls{CTA} high-level data via the \gls{VO}}

Each of the stages of the use case may be addressed by ESAP as follows:

\begin{itemize}

\item{\emph{User navigates to \pgls{ESAP}.}
  \begin{itemize}
    \item{\pgls{ESAP} is conveniently accessible through its web user interface; \cref{sec:vision:capabilities:ui}.}
  \end{itemize}
}

\item{\emph{User finds \gls{CTA} data via the \gls{ESAP} \gls{VO} service.}
  \begin{itemize}
    \item{The services accessible through a given instance of \pgls{ESAP} are not fixed, as described in \cref{sec:vision:model}; not every instance of \pgls{ESAP} will necessarily provide a \gls{VO} service.}
    \item{However, the \pgls{ESAP} data discovery system (\cref{sec:vision:capabilities:data}) is explicitly being designed with the ability to query the \gls{VO} in mind.}
    \item{It is expected that \gls{ESAP} will ship with \gls{VO} support in the core deliverable (\cref{sec:vision:extensibility}).}
  \end{itemize}
}

\item{\emph{The data is then combined with other data using \gls{VO} tools (e.g.\ Aladin lite)}
  \begin{itemize}
    \item{\pgls{ESAP}'s “shopping basket” (\cref{sec:vision:capabilities:dm}) is explicitly designed to make it straightforward to collect data from multiple sources and dispatch it to e.g.\ \gls{IDA} environments.}
    \item{\pgls{ESAP}'s \gls{SAMP} capability, \cref{sec:vision:capabilities:samp}, is designed to enable interoperability with \gls{VO} tools.}
  \end{itemize}
}

In short, all aspects of this use case which are of relevance to \pgls{ESAP} are addressed in the project's vision (\S\ref{sec:vision}), although service availability may vary with \pgls{ESAP} configuration.

\end{itemize}

\subsection{\glsentrytext{JIVE}}

The stated goal of this use case is that \emph{the scientist can find and read \gls{EVN} data through \pgls{ESAP} and perform analyses on (public) data sets}.
Specific items listed are:

\begin{itemize}

  \item{\emph{Provide \gls{EVN} data sets in the \gls{VO}.}}
  \item{\emph{Offer a Jupyter notebook for standard calibration and data reduction in the \gls{OSSR}.}}
  \item{\emph{Offer a radio-astronomy-specific Jupyter kernel based on the \gls{CASA} \autocite{casa:2007} project libraries and tools for handling of radio astronomy data.}}
  \item{\emph{Provide a JupyterHub service at \gls{JIVE} where users can execute the notebook close to the EVN archive, hosted at \gls{JIVE}}}

\end{itemize}

The following functionality is required from \pgls{ESAP} to address these goals:

\begin{itemize}

  \item{\pgls{ESAP} must be able to discover EVN data.
    \begin{itemize}
      \item{Data discovery is explicitly part of \pgls{ESAP}, as discussed in \cref{sec:vision:capabilities:data}.}
      \item{It is expected that \gls{ESAP} will ship with \gls{VO} support in the core deliverable (\cref{sec:vision:extensibility}).}
    \end{itemize}
  }

  \item{\pgls{ESAP} must be able to access Jupyter notebooks made available through the \gls{OSSR}.
    \begin{itemize}
      \item{This is an explicit goal of \pgls{ESAP}'s \gls{IDA} system, as described in \cref{sec:vision:capabilities:ida}.}
      \item{The current prototype is able to start the execution of Jupyter notebooks, but \gls{OSSR} integration is still a work-in-progress, as described in \cref{sec:current:ida}; a preliminary version of that functionality is expected in the third quarter of 2021.}
    \end{itemize}
  }
  \item{\pgls{ESAP} must be able to access the Jupyter-CASA kernel made available through the \gls{OSSR}.
    \begin{itemize}
      \item{Software and services would be sourced from the \gls{OSSR} and included in the environment by \pgls{ESAP}.}
    \end{itemize}
  }
  \item{\pgls{ESAP} must be able to access the JupyterHub service made available through the \gls{OSSR}.
    \begin{itemize}
      \item{Software and services would be sourced from the \gls{OSSR} and included in the environment by \pgls{ESAP}.}
    \end{itemize}
  }

\end{itemize}

In short, all aspects of this use case which are of relevance to \pgls{ESAP} are addressed in the project's vision (\S\ref{sec:vision}), although service availability may vary with \pgls{ESAP} configuration.

\subsection{KM3NeT}

\subsubsection{\glsentrytext{ANTARES} data set analysis}

The stated goal of this use case is that \emph{legacy data from \gls{ANTARES} is provided by KM3NeT and used as KM3NeT full analysis example}.
Specific items listed are:

\begin{itemize}
\item{\emph{Offer \gls{ANTARES} data through \gls{VO}.}}
\item{\emph{Create and offer \gls{ANTARES} example notebook.}}
\item{\emph{Offer data services for data interpretation.}}
\end{itemize}

The following functionality is required from \pgls{ESAP} to facilitate these goals:

\begin{itemize}

  \item{\pgls{ESAP} should be able to discover data in the \gls{VO}.
    \begin{itemize}
      \item{Data discovery is explicitly part of \pgls{ESAP}, as discussed in \cref{sec:vision:capabilities:data}.}
      \item{Support for the \gls{VO} is explicitly planned (\cref{sec:vision:extensibility}).}
      \item{The current prototype integrates with ASTRON VO services only (\cref{sec:current:data}), but this will be extended with future development.}
    \end{itemize}
  }

  \item{\pgls{ESAP} should facilitate an interactive analysis service, including Jupyter notebooks.
    \begin{itemize}
      \item{This is an explicit goal of \pgls{ESAP}'s \gls{IDA} system, as described in \cref{sec:vision:capabilities:ida}.}
      \item{The current prototype is able to start the execution of Jupyter notebooks; work on deeper integration is ongoing.}
    \end{itemize}
  }

\end{itemize}

In short, all aspects of this use case which are of relevance to \pgls{ESAP} are addressed in the current \pgls{ESAP} vision; development to provide full functionality is ongoing.

\subsubsection{KM3NeT data releases}

The stated goal of this use case is that \emph{the scientist can find and read KM3NeT event data through \pgls{ESAP} and perform analyses on public data sets}.
Specific items listed are:

\begin{itemize}

  \item{\emph{Provide data set in data lake.}}
  \item{\emph{Offer Jupyter notebook for dummy analysis in the \gls{OSSR}.}}
  \item{\emph{Offer KM3NeT-specific libraries for handling of data.}}
  \item{\emph{Check use of Rucio in KM3NeT.}}

\end{itemize}

The following functionality is required from \pgls{ESAP} to address these goals:

\begin{itemize}

  \item{\pgls{ESAP} must be able to discover and stage data in the (Rucio-based) data lake.
    \begin{itemize}
      \item{Data discovery is explicitly part of \pgls{ESAP}, as discussed in \cref{sec:vision:capabilities:data}.}
      \item{Support for Rucio is explicitly planned (\cref{sec:vision:extensibility}); an advanced prototype exists, but it is not currently deployed for use (\cref{sec:current:data}).}
      \item{Further Rucio integration is expected in the second half of 2021.}
    \end{itemize}
  }

  \item{\pgls{ESAP} must be able to access Jupyter notebooks made available through the \gls{OSSR}.
    \begin{itemize}
      \item{This is an explicit goal of \pgls{ESAP}'s \gls{IDA} system, as described in \cref{sec:vision:capabilities:ida}.}
      \item{The current prototype is able to start the execution of Jupyter notebooks, but \gls{OSSR} integration is still a work-in-progress, as described in \cref{sec:current:ida}; a preliminary version of that functionality is expected in the third quarter of 2021.}
    \end{itemize}
  }

\end{itemize}

In short, all aspects of this use case which are of relevance to \pgls{ESAP} are addressed in the current \pgls{ESAP} vision, and prototype versions of currently-unavailable functionality are expected shortly.

\subsection{\glsentrytext{LOFAR}}

Note that \gls{LOFAR} is not an \gls{ESFRI}, but nevertheless provides a number of important use cases that are tracked through the \gls{ESCAPE} project platform, and it serves as an important precursor to the \gls{SKA}.

\subsubsection{LOFAR0001: Long haul ingestion and replication}

This use case has no direct impact on \pgls{ESAP} development.

\subsubsection{LOFAR002: Data Processing}

The stated goal of this use case is that \emph{the ability to process data that is in the data lake at an external location}.

The following \pgls{ESAP} functionality is relevant:

\begin{itemize}

  \item{\pgls{ESAP} must be able to discover and stage data in the (Rucio-based) data lake.
    \begin{itemize}
      \item{Data discovery is explicitly part of \pgls{ESAP}, as discussed in \cref{sec:vision:capabilities:data}.}
      \item{Support for Rucio is explicitly planned (\cref{sec:vision:extensibility}); an advanced prototype exists, but it is not currently deployed for use (\cref{sec:current:data}).}
      \item{Further Rucio integration is expected in the second half of 2021.}
    \end{itemize}
  }

  \item{\pgls{ESAP} must be able to schedule interactive or batch processing workflows on data discovered in the data lake.
    \begin{itemize}
      \item{These are explicit goals of \pgls{ESAP} development, as described in \cref{sec:vision:capabilities:ida,sec:vision:capabilities:batch}.}
      \item{Current \gls{IDA} capabilities are described in \cref{sec:current:ida}.}
      \item{Batch computing capabilities are not currently available in \pgls{ESAP}, but their development is expected during late 2021.}
    \end{itemize}
  }

  \item{\pgls{ESAP} must be able to (visually) inspect relevant intermediate and final results.
    \begin{itemize}
      \item{These are explicit goals of \pgls{ESAP} development, as described in \cref{sec:vision:capabilities:ida}.}
      \item{Current \gls{IDA} capabilities are described in \cref{sec:current:ida}.}
    \end{itemize}
  }

\end{itemize}

In summary, all aspects of this use case which are of relevance to \pgls{ESAP} are addressed in the current \pgls{ESAP} vision, and prototype versions of currently-unavailable functionality are expected shortly.

\subsubsection{LOFAR003: Integration of the existing (LOFAR) \glsentrytext{LTA} in the data lake}

This use case has no direct impact on \pgls{ESAP} development.

\subsection{\glsentrytext{SKA}}

\subsubsection{\glsentrytext{SKA} global data lake proof-of-concept for astronomy-scale data products}

This use case has no direct impact on \pgls{ESAP} development.

\subsubsection{JHub notebook access to data lake data products}

The goals for this use case may be addressed as follows:

\begin{itemize}
  \item{\emph{Store \gls{SKA} data in data lake managed by Rucio.}
    \begin{itemize}
      \item{This has no direct impact on \pgls{ESAP}.}
    \end{itemize}
  }
  \item{\emph{Retrieve this data from data lake into a Notebook environment.}
    \begin{itemize}
      \item{\pgls{ESAP} will provide data-discovery tools for use the data lake, as described in \cref{sec:vision:capabilities:data,sec:vision:extensibility}}.
      \item{\pgls{ESAP}'s Jupyter integration will make it possible to seamlessly access that data from a notebook environment, as described in \cref{sec:vision:capabilities:ida}.}
    \end{itemize}
  }
  \item{\emph{User logs in to JupyterHub via \gls{ESCAPE} \gls{IAM}, and is then able to interact with the data lake seamlessly without re-uploading credentials.}
    \begin{itemize}
      \item{As described in \cref{sec:current:ida}, in the current prototype implementation it is necessary for the user to supply credentials, in the form of a token, directly into the notebook environment.
           This limitation will be addressed in a future development.}
    \end{itemize}
  }
\end{itemize}

In short, all aspects of this use case which are of relevance to \pgls{ESAP} are addressed in the current \pgls{ESAP} vision, and prototype versions of currently-unavailable functionality are expected shortly.

\subsubsection{Data product replica prepared for compute on request, interactive session started}

The goals for this use case may be addressed as follows:

\begin{itemize}

  \item{\emph{Identify Rucio data via \gls{ESAP}, take the data identifier to a JupyterHub server running the Rucio-JupyterLab extension environment, download it, calculate checksum.}
    \begin{itemize}
      \item{Identifying data in Rucio is covered in \cref{sec:vision:capabilities:data,sec:vision:extensibility}.}
      \item{Integration of \pgls{ESAP} with Jupyter systems is addressed in \cref{sec:vision:capabilities:ida}.}
      \item{Analysis within the notebook (e.g.\ calculation of checksums) falls outside the scope of \pgls{ESAP} itself, but should certainly be possible.}
    \end{itemize}
  }
  \item{\emph{Do above but with a custom Docker image for the user's environment (via BinderHub).}
    \begin{itemize}
      \item{As above, integration with Jupyter notebook systems, including flexible and comprehensive treatment of the user environment, will be supported by \pgls{ESAP}.}
      \item{Implementation details are not yet finalized; the details of the technology stack (BinderHub, Docker, etc) may vary from those requested, while still satisfying the scientific goals of the use case.}
    \end{itemize}
  }
  \item{\emph{Compute to data model: interactive service (JupyterHub) is dynamically launched at data location.}
    \begin{itemize}
      \item{\pgls{ESAP} itself will use standardized \glspl{API} to provide access to services which have been configured at remote sites.
            The detailed mechanism by which the service is instantiated at the remote site is outside \pgls{ESAP}'s scope.}
    \end{itemize}
  }
  \item{\emph{Rucio data identified via \gls{ESAP} that is not close to any JupyterHub service.
        \gls{ESAP} creates Rucio rule to move data as \gls{QoS} transition and user is sent to Jupyter server as before.}
    \begin{itemize}
      \item{As described in \cref{sec:vision:capabilities:data}, it is expected that \pgls{ESAP} will provide a capability for “staging” data to locations which are appropriate for processing.}
      \item{Development of this capability is still being planned, and the implementation may differ in technical details to that which is proposed in this use case.}
    \end{itemize}
  }
\end{itemize}

This use case proposes a number of detailed technical approaches.
While these are, in general, aligned with current \pgls{ESAP} plans, details of the final implementation may vary somewhat from the details expressed in the use case, without compromising the scientific usefulness of the solutions.

Members of the \pgls{ESAP} development team will continue to engage with colleagues from WP2/\gls{DIOS} and the \gls{SKA} to ensure that a properly-functional, fully-integrated solution to address this use case is delivered.
