\section{Introduction}
\label{sec:intro}

This document is \gls{ESCAPE} project deliverable \docNumber{}: “\docTitle{}” \autocite{ESCAPE-GA}.

\Pgls{ESAP} is the primary deliverable of \gls{ESCAPE} \Acrlong{WP} 5 (\Acrshort{WP}5\glsunset{WP}).
A primarily web-based application, it serves a key part of the interface between the services delivered by \gls{ESCAPE} and the scientific community, providing mechanisms by which users can discover, access, and analyze the data, workflows, and services which the \glspl{ESFRI} affiliated with \gls{ESCAPE}, and other projects, publish to the \gls{EOSC}.

At the time of writing, the \gls{ESCAPE} project is approximately at its mid-point.
A substantial amount of planning and development has been undertaken, and the project is now looking to consolidate on that start, by demonstrating increasing integration of the various services that have been developed, and by showing how they can be used to address the various use cases which are advanced by the associated \glspl{ESFRI}.
This makes it an opportune moment to step back and assess the current state and future plans for \gls{ESAP} development.
It is that analysis which is undertaken in this document.

This document assess the performance of \pgls{ESAP} by considering:

\begin{enumerate}

\item{To what extent the prototype meets the requirements which were specified in \citetitle{ESCAPE-D5.2} \autocite{ESCAPE-D5.2}?}

\item{To what extent the prototype meets the needs currently being expressed by the \gls{ESCAPE}-affiliated \glspl{ESFRI}, as expressed through the use cases currently being collated by the project?}

\item{What other goals or desires for \pgls{ESAP} might the community express through a workshop and accompanying call for feedback?}

\end{enumerate}

Note that none of these topics directly address “performance” as narrowly defined in terms of raw throughput or latency.
In practice, \pgls{ESAP} is unlikely ever to be a limiting factor here: it acts merely as a conduit to help users interact with other systems.
Those other systems handle bulk data storage, transport and compute; they do not place substantial load on \pgls{ESAP} itself.
Furthermore, while the \pgls{ESAP} system itself has been designed to be scalable where appropriate, at this stage in development we have focused on prototyping and exploring capabilities, rather than raw throughput.
In short, therefore, a raw computational performance measurement is not regarded as being significant for the terms of this report.

This report is structured as follows.
It starts by describing the vision for \pgls{ESAP} in \cref{sec:vision}.
This outlines how the ultimate \pgls{ESAP} deliverable is foreseen: what will it do, and how will it be deployed?
In \cref{sec:current}, it moves on to describe the current state of \pgls{ESAP} development.

Those first two sections are the essential background to the second part of the report, which directly attempts to answer the questions outlined above.
In particular, \cref{sec:requirements} tabulates the documented requirements and compares them to current development; \cref{sec:usecases} discusses how \pgls{ESAP} relates to all the use cases collected in the \gls{ESCAPE} project platform; and \cref{sec:workshop} describes a workshop that was called to solicit community feedback.
Finally, a summary of the results of this analysis is presented in \cref{sec:conclusions}.
