\section{Workshop}
\label{sec:workshop}

The \emph{Second \gls{WP}5 Workshop to Analyse Prototype Performance} was held on 5 August 2021.
This workshop was convened to satisfy \gls{ESCAPE} project milestone MS31.
The report submitted to mark the completion of this milestone is provided in \cref{app:ms31}.

At this workshop, participants were presented with a summary of the current status of \pgls{ESAP}, of future development plans, and of the status of integration with other \gls{ESCAPE} work packages.
Their feedback was directly solicited in the workshop, and they were given the opportunity to follow up after the workshop either through a structured questionnaire or by e-mail directly to the \gls{WP}5 Coordinator.

This section provides an overview of the workshop, a summary of the material presented, and notes on relevant discussions and feedback.
It concludes, in \cref{sec:workshop:lessons}, with some reflections on workshop organization and improvements which might be implemented for future workshops in a similar vein.

\subsection{Logistics}
\label{sec:workshop:logistics}

The workshop was intended to solicit opinions and feedback from across the \gls{ESCAPE} project, with a particular focus on the various \gls{ESFRI} stakeholders in \pgls{ESAP} development.
There was no formal advertisement or registration procedure; instead, announcements of the workshop were provided to:

\begin{itemize}

  \item{The \gls{ESCAPE} Executive Board;}
  \item{The \gls{ESCAPE} Technical Coordination Team, which includes representatives from the \glspl{ESFRI} and all of the \gls{ESCAPE} work packages;}
  \item{The \gls{ESCAPE} Work Package 5 membership.}

\end{itemize}

Announcements made the scope and intended audience of the workshop clear, and encouraged recipients to invite others (in particular, members of their \gls{ESFRI} or work package) who might be interested to attend the workshop.

The workshop took place on the morning of 5 August 2021.
Given the ongoing Covid-19 pandemic, the workshop took place on Zoom\footnote{\url{https://www.zoom.us}}; no “hybrid” or in-person option was available.

The workshop was scheduled for three hours, including discussion time.
In practice, substantially more time was needed; this is discussed in \cref{sec:workshop:lessons:discussion}.
The INDICO page at \url{http://indico.in2p3.fr/e/SecondWP5Workshop} provides the complete agenda for the workshop, as well as slides and other supporting material for the various presentations.

The number of participants connected to the workshop fluctuated with time, with a maximum of 47 people connected.
These were drawn from across the various \gls{ESCAPE} project work packages and the associated \glspl{ESFRI}.
\Cref{sec:workshop:lessons:zoom} briefly reflects on the level and type of participation in the workshop.

Following the workshop, participants were asked to complete a questionnaire with their feedback.
This is discussed in \cref{sec:workshop:questionnaire}.

\subsection{Meeting Summary}
\label{sec:workshop:discussion}

The agenda of the meeting was broadly divided into three parts, which are discussed separately here.

\subsubsection{Project Overview}

\paragraph{\Acrshort{ESAP} Overview (Swinbank)} provided an overview of the vision and current state of development of \pgls{ESAP}; the material broadly paralleled that presented in \cref{sec:vision,sec:current} of this document.
This included an extensive live demonstration of the current capabilities of \gls{ESAP}.

The response from the audience was broadly positive.
Key outcomes of the discussion included:

\begin{itemize}

  \item{The project should be careful not to redevelop user interfaces which already exist, especially where those extant interfaces have been carefully developed to facilitate particular science cases or user needs.
        This was especially emphasized for working with \Acrshort{ESO} archives: while it is important to facilitate working with them through \gls{ESAP}, it is necessary to avoid investing development effort which broadly duplicates the work which \Acrshort{ESO} has already done.
        The discussion focused around how \gls{ESAP} can use \gls{VO} technologies, notably \gls{SAMP}, in facilitating interactions between services wherever possible.}

  \item{The project should carefully track how \gls{ESAP} development is related to the use cases expressed by the \glspl{ESFRI}.
        \gls{WP}5 acknowledges this input; see, for example, \cref{sec:usecases} in this document.}

  \item{The project should consider the future of this work in the context of the \Acrshort{EOSC}-Future project.
        \gls{WP}5 acknowledges this input, and will continue to work with \gls{ESCAPE} leadership on this point.}

  \item{It was noted that the description of \gls{IDA} services (\cref{sec:vision:capabilities:ida,sec:current:ida} focuses on Jupyter-based systems.
        This is primarily driven by the use cases that have been expressed by the various project stakeholders (\cref{sec:usecases}).
        However, as described in \cref{sec:vision}, \pgls{ESAP} is designed to be extensible to other service types as the need arises.
        Similarly, the \gls{OSSR} expects to register a range of different software and service types.}

\end{itemize}

\paragraph{\Acrshort{ESAP} Architecture (Vermaas)} provided a more technical deep-dive into how services can be integrated with the \gls{ESAP} system.
This included essential background material for the overview presented earlier.

\subsubsection{Future Development Plans}

\paragraph{Batch Processing (Hughes)} gave an overview of the current status of, and plans for further developing, batch processing capabilities within \pgls{ESAP}.
Refer to \cref{sec:vision:capabilities:batch,sec:vision:extensibility} of this document for an overview of these plans.

A key aim of this talk was to solicit guidance from the community about which batch systems they regard as a priority, as well as developer effort to help integrate \pgls{ESAP} with their own systems.
The \gls{WP}5 leadership adopts the position that selection of a batch system or systems should be largely community driven: rather than \gls{WP}5 choosing a standardized system, \gls{WP}5 will work with the community to enable whatever batch systems they require (within the limits of available resources).

The primary advocates of \pgls{ESAP} interfaces with batch computing come from \gls{CTA}, who require interfaces with DIRAC\@.
We agreed that development would prioritize this work; members of the community were encouraged to work with \gls{WP}5 developers to push this effort forward.

\paragraph{Managed Database (Chanial)} discussed the Managed Database service being proposed for \pgls{ESAP} (\cref{sec:vision:capabilities:db}), including an impressive live demo.
The discussion focused around technical aspects of the system: the technologies used, and how they could be integrated with the rest of the \gls{ESAP} system.

The suggestion of the Managed Database service being used as the basis for potential improvements to the shopping basket (\cref{sec:vision:capabilities:dm}) was discussed.
This could unlock a range of new possibilities.
However, the Managed Database is currently an immature prototype, and the work package is cognizant of its commitments to make robust deliveries of promised functionality on time.
The current plan is therefore to continue development of the existing shopping basket system, while working on the Managed Database service in parallel and remaining alert to opportunities for future enhancements.

Interested members of the community were encouraged to connect directly with the \gls{WP}5 developers to provide additional use cases and guide the development effort.

\subsubsection{Interactions with Other Work Packages}

\paragraph{\Acrshort{DIOS} / Work Package 2 (Grange, Di Maria, Hilmy)} demonstrated the “data lake as a service”: a powerful interface between a Jupyter notebook service and the Rucio-based data lake.
There was general agreement that this represented a key capability, and that plans should proceed for integrating it with the overall \pgls{ESAP} service offering.
Additional discussion focused on technical aspects of the data management.

\paragraph{\Acrshort{OSSR} / Work Package 3 (Hughes, Voutsinas, Graf)} provided an extensive introduction to how software can be registered with the \gls{OSSR} (“onboarding”), then moved on to discuss the current state of and future plans for integration between the \gls{OSSR} and \gls{ESAP}, as discussed in \cref{sec:vision,sec:current} of this document.
A number of important questions for future development were raised, and this workshop resolved to schedule further close discussion between WP3 and \gls{WP}5 members to clarify interfaces and goals.
However, there was general agreement about the overall direction of travel and goals for this work.

\paragraph{\Acrshort{VO} / Work Package 4 (Grange)} provided a brief overview of \pgls{ESAP} integration with the \gls{VO}.
The discussion linked closely back to the earlier discussion regarding interface duplication: the consensus was that \gls{VO} technologies like \gls{SAMP} and VOTable \autocite{2019ivoa.spec.1021O} offer important opportunities for \gls{ESAP} to interface between various different services without simply duplicating their existing interfaces.
However, effectively taking advantage of \gls{VO} technologies requires careful attention to metadata management: \gls{ESAP} developers must be cognizant of this and integration must be treated with care.

\subsection{Questionnaire}
\label{sec:workshop:questionnaire}

At the workshop, participants were directed to a questionnaire which was hosted on Google Forms\footnote{\url{https://docs.google.com/forms}}.
The questionnaire was designed to provide a structured way to collect feedback from the workshop participants about what they heard at the meeting: after a period of reflection and time to experiment with a “live” \pgls{ESAP} test system, they would be able to record their considered opinion.

The complete questionnaire as sent to users is shown in \cref{app:questionnaire} of this document.

In practice, only two users completed the questionnaire.
This result is obviously somewhat disappointing; possible reasons and alternative approaches which make be taken at future workshops are discussed in \cref{sec:workshop:lessons:questionnaire}.

Highlights of the responses received were:

\begin{itemize}

  \item{One (anonymous) response which indicated an intention to deploy \pgls{ESAP} on their own local infrastructure;}
  \item{An emphasis on the necessity to define an \gls{API} for connecting \pgls{ESAP} with batch compute systems;}
  \item{A desire to enable computing without duplicating or staging data when dealing with extremely large datasets;}
  \item{Suggestions for streamlining some aspects of the \pgls{ESAP} interface.}

\end{itemize}

Given the small number of responses received, the questionnaire is clearly of limited value in understanding the response of the community to \pgls{ESAP}.
However, the overall level of participation in the workshop and the quality of the resulting discussion provide a high level of confidence in its validation of the \pgls{ESAP} platform, so this is not regarded as a major concern.
The \gls{WP}5 team will continue to use a variety of different methodologies to solicit input from stakeholders over the remainder of the project duration.

\subsection{Outcomes}
\label{sec:workshop:outcomes}

As recorded above, there were a number of insightful points raised during discussion at the workshop. In particular, the emphasis on avoiding simple reimplementation or duplication of existing user interfaces was well taken, as were the thoughts on how best to make use of \gls{VO} technologies.
The \gls{WP}5 team will schedule a number of follow-up discussions --- internally, cross-work-package, and with other stakeholders --- to develop plans in response to the feedback received.

Overall, though, the workshop provides strong validation of the current plans and implementation of \pgls{ESAP}: while there were many suggestions for improvement and minor course updates, there were no substantial objections raised to any of the plans discussed, and nor were there major gaps identified that the \pgls{ESAP} vision does not adequately account for.

\subsection{Lessons Learned for Future Workshops}
\label{sec:workshop:lessons}

\subsubsection{Zoom}
\label{sec:workshop:lessons:zoom}

At this stage in the global Covid-19 pandemic, workshop participants are familiar with the Zoom platform; few technical issues were reported, and the workshop organizers are not aware of anybody who was unable to participate due to issues with the technology.

It is likely that holding the workshop on Zoom increased the number of participants: it is unlikely that as many people would have been able to join the workshop if it involved travel to meet in person.

On the other hand, this also means that many of the participants seemed disengaged.
A relatively small fraction of the participants actively took part in the debate; many others likely connected for a few minutes without investing substantial attention

Future workshops could seek to mitigate this by more actively soliciting participation in the discussion: rather than simply calling for questions or comments at the end of talks, an active chair could request feedback from participants, or otherwise structure the discussion to ensure that all participants can fully participate.

\subsubsection{Discussion Time}
\label{sec:workshop:lessons:discussion}

The workshop was scheduled for a total of three hours, which included a number of presentations and time set aside for discussion (see the agenda in \cref{sec:workshop:logistics}).
In practice, this was not adequate to the amount of discussion which arose.
Effectively all of the scheduled talks engendered discussion which over-ran the slot allocated to the talk.
The final talk did not finish until the nominal end time of the workshop, leaving no (scheduled) time for the final discussion session.
In practice, discussion continued for 45 minutes after the workshop among those who did not have to leave for other commitments.
While this additional discussion time was valuable, it meant that the views of those who did have to leave were excluded from full consideration.

\subsubsection{Live Demonstrations and Test Systems}
\label{sec:workshop:lessons:demo}

Three live demonstrations were run during the workshop.
These worked surprisingly well: there were no significant technical problems, and holding the workshop on Zoom meant that everybody had a clear, full screen view of what was being displayed.
Feedback on all of these sessions was very positive, and nobody reported any difficulties in viewing or understanding the material.

A test system was also made available which participants could connect to and experiment for themselves.
Little obvious engagement was seen with this system: no user feedback was received based on their experiences with it and no bugs or problems were reported.

Combined with other lessons (notably \cref{sec:workshop:lessons:questionnaire}), this is indicative of the challenges involved in maintaining user engagement after the formal end of the workshop programme: participants are eager to participate in discussions during the scheduled workshop, but will then move on to other activities rather than follow-up on workshop activities.

\subsubsection{Questionnaire}
\label{sec:workshop:lessons:questionnaire}

Only two responses were received to the questionnaire that workshop participants were asked to complete (\cref{sec:workshop:questionnaire}), and no other feedback was sent to the \gls{WP}5 Coordinator.

This speaks to the same difficulties in maintaining user engagement as seen in \cref{sec:workshop:lessons:demo}.
It may also indicate that the questionnaire was too detailed: rather than participants being able to quickly submit their thoughts, they were forced to click through multiple pages, and may have moved on to other high-priority tasks before finishing.

One immediate lesson learned is that the workshop organizers should be more proactive about reminding the audience of the existence of the questionnaire.
Although it was repeatedly mentioned during the meeting itself, there was no follow-up after the meeting.
The response rate would likely have been improved by sending reminders directly to workshop participants a few days after the meeting.

Taken in concert with \cref{sec:workshop:lessons:zoom} and \cref{sec:workshop:lessons:discussion}, this suggests that efforts should be made to build structured audience feedback into the agenda of the workshop.
Rather than facilitating free-form discussion and then relying on post-workshop input, like a questionnaire, for fully-considered audience feedback, the focus should be on giving the audience time and opportunity to record mature opinions during the workshop itself.
